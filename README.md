
# NicoWeather

## Architecture
The architecture of this app is done in a way that can be evolved and scaled into a bigger app. I could have chosen an easier way to implement this basic functionality (a View controller that performs API calls using Alamofire directly), but I wanted to create a skeleton that allows the app to grow. Often apps start with a small team of developers but eventually, a much bigger team ends up working on the same code. It is good if you structure the app in a way that can grow from the beginning. The app has three different layers that are totally independent of each other.

### Network layer.
This layer contains all the classes that are able to load JSON from the network. It is generic and it can load any request from the network, it is not even tided to Weather API itself, this part of the code could work in any app that needs to load JSON from the network. `APIClient.swift` is the main class of this layer. It defines its own request model, returns an already deserialized JSON Dictionary and it executes everything in a background thread and returns the JSON response using a block.

##### Things that can improve in this layer.
  - At this point, I am not doing anything with the cache and I am relaying on `Alamofire` and `URLSession` to do that for me. In case we wanted to implement a special caching system we would have to do it in this layer. *Example: I have seen APIs that do not contain caching headers if so, we should implement our own client caching system*.

### Data layer.
In this example, this layer only has two `DataSources` since we only needed to implement a couple API calls though, it can easily contain more classes like this one. All `DataSources`  should follow exactly the same structure:
 - Each `DataSource` defines its independent request and response. This way everything is encapsulated and this `DataSource` can work anywhere. This also allows us to unit test this class in an easy way.
 - It does not need to be a singleton since the network layer is already taking care of this.
 - No need to take care of background threading, Network layer will take care of it already.

##### Things that can improve in this layer.
 - Serialization of the models as: `City` or `Forecast` happens within the `DataSource`. If we had more endpoints using the same data model, we should abstract this logic into a serializer class.
  - Data serialization (creation of the models) and business logic (removing the wrappers that we do not support) are in the same place. In case our business logic was much bigger, we could move all that code into a different places.

### Testing our Data Layer and Network Layer
As you can see the in the code, most of the code in the Network Layer and the Data Layer are tested by unit tests. Nothing of this code imports `UIKit` which makes testing much easier.

##### Things that can improve in this layer.
 - We should include more tests so we have a better coverage.

 ### UI layer
This UI layer is the only layer in the app which imports `UIKit`. It will create all the view controllers and views that we are going to use within the app. In this case, the app is really simple with just two view Controllers. 

### Small video of the app
![Gif](Resources_no_xcode/video_demo.gif)