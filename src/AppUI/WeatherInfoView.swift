//
//  WeatherInfoView.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

class WeatherInfoView: UIView {

    @IBOutlet weak var bigWeatherInfoImageView: UIImageView!
    @IBOutlet weak var rainImageView: UIImageView!
    @IBOutlet weak var windIconImageView: UIImageView?
    @IBOutlet weak var rainIconImageView: UIImageView?

    @IBOutlet weak var mainInformationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel?
    @IBOutlet weak var windInfoLabel: UILabel?
    @IBOutlet weak var tempMaxMinLabel: UILabel?
    @IBOutlet weak var temperatureInfoLabel: UILabel!
    
    private class func instanceFromNib(nibName: String) -> WeatherInfoView {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! WeatherInfoView
    }
    
    class func bigInstanceFromNib() -> WeatherInfoView {
        return instanceFromNib(nibName: "WeatherInfoView")
    }
    
    class func miniInstanceFromNib() -> WeatherInfoView {
        return instanceFromNib(nibName: "WeatherInfoViewMini")
    }
    
    public func setCloudiness(cloudiness: Int) {
        if cloudiness < 20 {
            self.bigWeatherInfoImageView.image = #imageLiteral(resourceName: "sun_big_icon")
        } else if cloudiness < 70 {
            self.bigWeatherInfoImageView.image = #imageLiteral(resourceName: "sunCloud_big_icon")
        } else {
            self.bigWeatherInfoImageView.image = #imageLiteral(resourceName: "cloud_big_icon")
        }
    }
    
    public func setRain(rain: Double?) {
        if rain == nil || rain! < 0.1 {
            self.rainImageView.isHidden = true
        } else if rain! < 1 {
            self.rainImageView.isHidden = false
            self.rainImageView.image = #imageLiteral(resourceName: "rain_drops_1")
        } else if rain! < 3 {
            self.rainImageView.isHidden = false
            self.rainImageView.image = #imageLiteral(resourceName: "rain_drops_2")
        } else {
            self.rainImageView.isHidden = false
            self.rainImageView.image = #imageLiteral(resourceName: "rain_drops_3")
        }
    }

}
