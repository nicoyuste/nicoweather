//
//  WeatherViewWrapperCollectionViewCell.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

class WeatherViewWrapperCollectionViewCell: UICollectionViewCell {
    var weatherView : WeatherInfoView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createWeatherView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.createWeatherView()
    }
    
    // MARK: Private helpers
    
    private func createWeatherView() {
        let weatherView = WeatherInfoView.miniInstanceFromNib()
        self.addSubviewAndMakeItSticky(subview: weatherView)
        self.weatherView = weatherView
    }
}
