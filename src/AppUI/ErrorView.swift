//
//  ErrorView.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/8/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

typealias ErrorViewClickListener = () -> Void

class ErrorView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var okButton: UIButton!
    var clickListenerBlock: ErrorViewClickListener?
    
    class func instanceFromNib() -> ErrorView {
        let errorView = UINib(nibName: "ErrorView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ErrorView
        
        errorView.okButton.clipsToBounds = false
        errorView.okButton.layer.borderColor = UIColor.white.cgColor
        errorView.okButton.layer.borderWidth = 1
        errorView.okButton.layer.cornerRadius = 20
        errorView.okButton.addTarget(errorView, action: #selector(okTapped), for: UIControlEvents.touchUpInside)
        
        return errorView
    }

    // MARK: Actions
    
    @objc private func okTapped() {
        if let block = self.clickListenerBlock {
            block()
        }
    }
}
