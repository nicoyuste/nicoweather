//
//  ViewController.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit
import CoreLocation
import NVActivityIndicatorView
import AFBlurSegue

class ViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDataSource, SettingsDidChangeDelegate {

    let reuseIdentifier = "collectionViewCellId"
    
    let locationManager = CLLocationManager()
    let dateFormatter = DateFormatter()
    var currentLocation : CLLocation?
    var forecast : Forecast?
    
    var mainWeatherView : WeatherInfoView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var currentLocationInfo: UILabel!
    @IBOutlet weak var weatherViewContainer: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBackgroundImage()
        self.mainWeatherView = self.createWeatherView()
        self.dateFormatter.dateFormat = "HH:00"
        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnteredForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkAndStartTrackingLocation()
    }
    
    @objc func applicationEnteredForeground() {
        self.checkAndStartTrackingLocation()
    }
    
    @objc func applicationDidEnterBackground() {
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: Actions
    
    @IBAction func settingsTapped(_ sender: UIButton) {
        self.performSegue(withIdentifier: "settingsSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let hvc = segue.destination as! SettingsViewController
        hvc.delegate = self
        hvc.modalTransitionStyle = .crossDissolve
        
        let blurSegue = segue as! AFBlurSegue
        blurSegue.blurRadius = 20
        blurSegue.tintColor = UIColor.clear
        blurSegue.saturationDeltaFactor = 0.2;
    }
    
    // MARK: Prepare UI
    
    private func showErrorView(title: String?, message: String?, buttonText: String?, completion: (()-> Void)?) {
        let errorView = ErrorView.instanceFromNib()
        errorView.center = self.view.center
        errorView.titleLabel.text = title
        errorView.messageLabel.text = message
        errorView.tag = kErrorViewTag
        errorView.okButton.setTitle(buttonText, for: UIControlState.normal)
        errorView.clickListenerBlock = completion
        self.view.addSubview(errorView)
    }
    
    private func createAndLoadingView() {
        let activityFrame = CGRect(x: 0, y: 0, width: 100, height: 100)
        let activityView = NVActivityIndicatorView(frame: activityFrame, type: NVActivityIndicatorType.orbit, color: kBlueColor, padding: nil)
        activityView.tag = kActivityViewTag
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        activityView.startAnimating()
    }
    
    private func setBackgroundImage() {
        let backgroundImageView = UIImageView(image: #imageLiteral(resourceName: "Background_image"))
        self.view.addSubviewAndMakeItSticky(subview: backgroundImageView)
        self.view.sendSubview(toBack: backgroundImageView)
    }
    
    private func createWeatherView() -> WeatherInfoView {
        let weatherView = WeatherInfoView.bigInstanceFromNib()
        self.weatherViewContainer.addSubviewAndMakeItSticky(subview: weatherView)
        weatherView.isHidden = true
        return weatherView
    }
    
    private func addInformationToWeatherView(weatherView: WeatherInfoView, weatherInfo: WeatherInfo) {
        
        let tempUnit = Temperature_UNIT.fromInt(value: UserDefaults.standard.object(forKey: USER_DEFAULTS_TEMPERATURE_KEY))!
        
        weatherView.isHidden = false
        weatherView.temperatureInfoLabel.text = weatherInfo.getTemperature(unit: tempUnit)
        weatherView.mainInformationLabel.text = weatherInfo.weatherDescription.capitalizingFirstLetter()
        weatherView.setCloudiness(cloudiness: weatherInfo.cloudiness)
        weatherView.setRain(rain: weatherInfo.rain)
        
        if  weatherView.tempMaxMinLabel != nil {
            weatherView.tempMaxMinLabel!.text = weatherInfo.getMaxMinTemperature(unit: .Celsius)
        }
        
        if  weatherView.windInfoLabel != nil {
            weatherView.windInfoLabel!.text = weatherInfo.getWindSpeed()
        }
        
        if  weatherView.timeLabel != nil {
            weatherView.timeLabel!.text = self.dateFormatter.string(from: weatherInfo.date)
        }
    }
    
    // MARK: Horizontal Collection View and Delegate
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let forecast = self.forecast {
            return forecast.weather.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WeatherViewWrapperCollectionViewCell
        let weatherInfo = self.forecast!.weather[indexPath.row]
        self.addInformationToWeatherView(weatherView: cell.weatherView, weatherInfo: weatherInfo)
        return cell
    }
    
    // MARKL: Settings Delegate
    
    func appSettingsDidChange() {
        
        guard let forecast = self.forecast else { return }
        self.collectionView.reloadData()
        self.addInformationToWeatherView(weatherView: self.mainWeatherView, weatherInfo: forecast.weather.first!)
    }
    
    // MARK: Location Manager and Delegate
    
    private func checkAndStartTrackingLocation() {
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                self.showErrorView(title: "Location service disabled.", message: "This app needs permissions to access your location.", buttonText: "Go to Settings", completion: {
                    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else { return }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    }
                    self.view.viewWithTag(kErrorViewTag)?.removeFromSuperview()
                })
            case .authorizedAlways, .authorizedWhenInUse:
                print("Location services are not enabled")
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
                locationManager.startUpdatingLocation()
                self.createAndLoadingView()
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.currentLocation = location
            self.lookForCity(location)
        }
    }
    
    // MARK: Api Calls
    
    func lookForCity(_ location: CLLocation) {
        let dataSource = CityFinderDataSource(apiClient: ISAPIClient.shared)
        let request = CityFinderRequest(location: location)
        dataSource.executeRequest(request: request) { response in
            switch response {
            case .success:
                let city = response.value!.first!
                print("We found the current city: \(city.name)")
                self.locationLabel.text = city.name
                self.currentLocationInfo.isHidden = false
                self.lookForForecastWithCity(city: city)
            case .failure:
                self.view.viewWithTag(kActivityViewTag)?.removeFromSuperview()
                self.showErrorView(title: "Server Error", message: "There was an error with the server.", buttonText: "Retry", completion: {
                    self.lookForCity(location)
                    self.view.viewWithTag(kErrorViewTag)?.removeFromSuperview()
                })
            }
        }
    }
    
    func lookForForecastWithCity(city : City) {
        let forecastDataSource = ForecastDataSource(apiClient: ISAPIClient.shared)
        let forecastRequest = ForecastRequest(cityId: String(city.id))
        forecastDataSource.executeRequest(request: forecastRequest) { response in
            switch response {
            case .success:
                self.forecast = response.value!
                self.addInformationToWeatherView(weatherView: self.mainWeatherView, weatherInfo: self.forecast!.weather.first!)
                self.collectionView.reloadData()
            case .failure:
                print(response.error!.localizedDescription)
                self.showErrorView(title: "Server Error", message: "There was an error with the server.", buttonText: "Retry", completion: {
                    self.lookForForecastWithCity(city:city)
                    self.view.viewWithTag(kErrorViewTag)?.removeFromSuperview()
                })
            }
            self.view.viewWithTag(kActivityViewTag)?.removeFromSuperview()
        }
    }
    
}

