//
//  SettingsViewController.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/8/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

protocol SettingsDidChangeDelegate {
    
    func appSettingsDidChange()
}


class SettingsViewController: UIViewController {

    var delegate : SettingsDidChangeDelegate?
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var temperatureSegment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        self.containerView.layer.cornerRadius = 30;
        self.containerView.layer.masksToBounds = true;
        
        self.loadInformationFromDefualts()
    }
    
    private func loadInformationFromDefualts() {
        let tempUnit = Temperature_UNIT.fromInt(value: UserDefaults.standard.object(forKey: USER_DEFAULTS_TEMPERATURE_KEY))!
        switch tempUnit {
        case .Celsius:
            self.temperatureSegment.selectedSegmentIndex = 0
        case .Farenheit:
            self.temperatureSegment.selectedSegmentIndex = 1
        }
    }
    
    private func notifyDelegate() {
        guard let delegate = self.delegate else { return }
        delegate.appSettingsDidChange()
    }
    
    // MARK: Actions
    
    @IBAction func temperatureUnitChanged(_ sender: Any, forEvent event: UIEvent) {
        
        guard let sender = sender as? UISegmentedControl else { return }

        switch sender.selectedSegmentIndex {
        case 0:
            UserDefaults.standard.set(Temperature_UNIT.Celsius.toInt(), forKey: USER_DEFAULTS_TEMPERATURE_KEY)
        case 1:
            UserDefaults.standard.set(Temperature_UNIT.Farenheit.toInt(), forKey: USER_DEFAULTS_TEMPERATURE_KEY)
        default:
            return
        }
        UserDefaults.standard.synchronize()
        self.notifyDelegate()
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
