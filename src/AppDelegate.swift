//
//  AppDelegate.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit
import Intents

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.requestSiriPermissions()
        self.setDefaultSettingsIfNotCreated()
        return true
    }
    
    /// This method handles the permissions with Siri.
    func requestSiriPermissions() {
        INPreferences.requestSiriAuthorization { status in
            if status == .authorized {
                print("Hey, Siri!")
            } else {
                print("Nay, Siri!")
            }
        }
    }
    
    /// This needs some configuration to be in the app right from the beggining.
    /// This method is used to set the defaults of that configuration data.
    func setDefaultSettingsIfNotCreated() {
        let tempUnit = Temperature_UNIT.fromInt(value: UserDefaults.standard.object(forKey: USER_DEFAULTS_TEMPERATURE_KEY))
        if tempUnit == nil {
            UserDefaults.standard.set(Temperature_UNIT.Celsius.toInt(), forKey: USER_DEFAULTS_TEMPERATURE_KEY)
            UserDefaults.standard.synchronize()
        }
        
    }
}

