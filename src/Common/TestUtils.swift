//
//  TestUtils.swift
//  weather-appTests
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation
import XCTest
@testable import weather_app

/// A class that is only used in our tests.
/// It is a normal API that any datasource will be able to use. In this case you will be able to mock the responses.
/// That way you can mock API responses and unit test datasources.
class mockAPIClient : NSObject, ISAPIClientProtocol {
    
    let returnSuccess:Bool!
    let jsonResponse:String!
    
    init(returnSuccess: Bool, jsonResponse:String) {
        self.returnSuccess = returnSuccess
        self.jsonResponse = jsonResponse
    }
    func executeRequest(request: ISAPIClientRequest, completionHandler: @escaping APIClientCompletionHandler) {
        if (returnSuccess) {
            if let data = self.jsonResponse.data(using: .utf8) {
                do {
                    let jsonDic = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    completionHandler(Result.success(jsonDic))
                } catch {
                    XCTFail("We had a problem loading the JSON")
                }
            }
        } else {
            completionHandler(Result.failure(testDataSourceError.testError))
        }
    }
    
}

let testWeirdJSON = "{\"data\":{\"NO_DATA\":\"NO_DATA\"}}"

enum testDataSourceError : Error {
    case testError
}

