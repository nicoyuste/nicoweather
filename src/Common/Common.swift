//
//  Common.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

/// Some of the constants that we are going to be using along the app.
// TODO: Some of the configuration data, as the WEATHER_API_ID could be loaded from things like Firebase.
// That way we could update the id without the need of a new version of the app.
let OPEN_WEATHER_API_ID : String = "4f384394ae4ddb4911aaf5b423e32fbc"
let USER_DEFAULTS_TEMPERATURE_KEY = "user.defaults.temperature.unit.v1"
let USER_DEFAULTS_WIND_KEY = "user.defaults.wind.unit.v1"

/// Main result enum that encapsulates both the success and the failure cases.
/// It is used in different places around the app.
public enum Result<Value> {
    case success(Value)
    case failure(Error)
    
    /// Returns the associated value if the result is a success, `nil` otherwise.
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Returns the associated error value if the result is a failure, `nil` otherwise.
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}
