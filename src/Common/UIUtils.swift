//
//  UIUtils.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

/// Some of the UI constants that we are going to need across the app.
let kBlueColor = UIColor(red:0.05, green:0.20, blue:0.60, alpha:1.00)
let kActivityViewTag = 345872
let kErrorViewTag = 345872

/// A UIView extensions.
extension UIView {
    
    /// Adds a view to self and adds the needed constraints so the subview is sticky to his parent.
    ///
    /// - Parameters:
    ///   - subview: The view that is going to be added to self.
    func addSubviewAndMakeItSticky(subview: UIView) {
        self.addSubview(subview)
        subview.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": subview]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": subview]))
    }
}
