//
//  StringExtensions.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// A String extension.
extension String {
    
    /// Modifies itself and capitalizes the first letter of the String.
    ///
    /// - Returns:
    ///   - This string.
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
