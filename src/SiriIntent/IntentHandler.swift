//
//  IntentHandler.swift
//  Siri Weather App
//
//  Created by Nicolas Yuste Tirados on 10/8/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit
import Intents

enum FetchWeatherError: Error {
    case noLocation
    case noForecastInLocation
}

class IntentHandler: INExtension {
    
    // MARK: - INIntentHandlerProviding methods
    
    override func handler(for intent: INIntent) -> Any {
        return self
    }
    
    // MARK: - Specific Intent Hanlder
    // Called when it is time to resolve the Siri Request
    
    func resolveWeatherPetition() {
        
        // First get location
        
        // Look for the city
        let dataSource = CityFinderDataSource(apiClient: ISAPIClient.shared)
        let request = CityFinderRequest(location: CLLocation(latitude: 5, longitude: 5)) // TODO: We need yo get real data.
        dataSource.executeRequest(request: request) { response in
            switch response {
            case .success:
                let city = response.value!.first!
                let forecastDataSource = ForecastDataSource(apiClient: ISAPIClient.shared)
                let forecastRequest = ForecastRequest(cityId: String(city.id))
                forecastDataSource.executeRequest(request: forecastRequest) { response in
                    switch response {
                    case .success:
                        print("Success")
                    // TODO: Return information to Siri
                    case .failure:
                        print("Error")
                        // TODO: Return error to Siri.
                    }
                }
            case .failure:
                print("Error")
                // TODO: Return error to Siri
            }
        }
        
        // Look for the weather
        
    }
}
    


