//
//  CityDataSource.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// Completion block type that ForecastDataSource fires when it finishs executing a request.
typealias ForcastDataSourceCompletionHandler = (Result<Forecast>) -> Void

/// This class is able to perform a search into the Open Weather API. It returns the Forecast for a give location
/// It will perform the API call asynchronously and it will call back into the completion block when finished.
class ForecastDataSource: NSObject {

    private let apiClient : ISAPIClientProtocol!
    
    /// Creates and returns a new Data Source object that is able to perform a search into Open Weather API.
    /// It requires an APIClient object in order to work. (We do this for testing porpouse so we can mock all the dependencies
    /// this class may have)
    ///
    /// - Parameter apiClient: The apiClient that is going to be able to execute the request.
    init(apiClient: ISAPIClientProtocol) {
        self.apiClient = apiClient
    }
    
    /// It executes a request. It will fire the completion block when it finishs.
    ///
    /// - Parameters:
    ///   - request: The request object that encapsulates all the request information
    ///   - completion: The completion that is going to be executed with finished.
    public func executeRequest(request: ForecastRequest, completion: @escaping ForcastDataSourceCompletionHandler)  {
        apiClient.executeRequest(request: request) { apiResponse in
            switch apiResponse {
            case .success:
                let forecast = self.generateForecast(jsonForecast: apiResponse.value!)
                if forecast.city == nil {
                    completion(.failure(ForecastDataSourceError.missingCity))
                } else if forecast.weather.count == 0 {
                    completion(.failure(ForecastDataSourceError.missingWeatherInfo))
                } else {
                    completion(.success(forecast))
                }
            case .failure:
                completion(.failure(apiResponse.error!))
            }
        }
    }
    
    // MARK: Private helpers
    
    private func generateForecast(jsonForecast : [String: Any]) -> Forecast {
        let result = Forecast()
        
        if let cityJson = jsonForecast["city"] as? [String: Any],
            let cityName = cityJson["name"] as? String,
            let cityId = cityJson["id"] as? Int {
            result.city = City(id: cityId, name: cityName)
        }
        
        if let weatherList = jsonForecast["list"] as? [[String:Any]] {
            for element in weatherList {
                guard
                    let timestamp = element["dt"] as? UInt64,
                    let weatherJson = element["weather"] as? [[String: Any]],
                    let mainJson = element["main"] as? [String: Any],
                    let windJson = element["wind"] as? [String: Any],
                    let cloudJson = element["clouds"] as? [String: Any],
                    let rainJson = element["rain"] as? [String: Any],
                    let weatherDecription = weatherJson[0]["description"] as? String,
                    let temperature = mainJson["temp"] as? Double,
                    let temperature_max = mainJson["temp_max"] as? Double,
                    let temperature_min = mainJson["temp_min"] as? Double,
                    let cloudiness = cloudJson["all"] as? Int,
                    let windSpeed = windJson["speed"] as? Double
                else { continue }
    
                result.weather.append(WeatherInfo(timestamp: timestamp,
                                                   temperature: temperature,
                                                   tempMax: temperature_max,
                                                   tempMin: temperature_min,
                                                   windSpeed: windSpeed,
                                                   weatherDescription: weatherDecription,
                                                   cloudiness: cloudiness,
                                                   rain: rainJson["3h"] as? Double))
            }
        }
        return result
    }
}

enum ForecastDataSourceError : Error {
    case missingForecast // Sent in we could not locate the user
    case missingWeatherInfo
    case missingCity
}
