//
//  CityRequest.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// A City Search request that can be executed by CityDataSource
/// It is uses to look for an actual city based on coordinates.
class ForecastRequest: NSObject, ISAPIClientRequest {
    
    private let cityId : String!
    
    // MARK: Init method
    
    /// Creates and returns a new CityRequest that can be executed by CityDataSource
    ///
    /// - Parameters:
    ///   - location: The location we want to use to find a location
    init(cityId: String){
        self.cityId = cityId
    }
    
    // MARK: - Implementing ISAPIClientRequest Protocol
    
    func getHost() -> String {
        return "https://api.openweathermap.org/"
    }
    func getMethod() -> ISAPIClientMethod {
        return .GET
    }
    func getPath() -> String {
        return "data/2.5/forecast"
    }
    func getBoby() -> Dictionary<String, Any>? {
        return nil
    }
    func getRequestHeaders() -> Dictionary<String, String>? {
        return nil
    }
    func getQueryParams() -> Dictionary<String, String>? {
        
        var params : Dictionary<String, String> = [:]
        params["id"] = cityId
        params["appid"] = OPEN_WEATHER_API_ID // TODO: We are going to download this one from Firebase.
        
        return params
    }
}
