//
//  WeatherInfo.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import UIKit

enum Temperature_UNIT {
    case Celsius
    case Farenheit
    
    /// Creates an returns an Int representation of this Temperature_UNIT
    ///
    /// - Returns:
    ///   - The Int the represents this Temperature_INIT
    func toInt() -> Int {
        switch self {
        case .Celsius:
            return 0
        case .Farenheit:
            return 1
        }
    }
    
    /// Class func.
    /// Returns the Temperature_UNIT value from an Int. It may return nil in case the Int does not represents.
    /// any Temperature_UNIT or in case value is nil.
    ///
    /// - Parameters:
    ///   - The Int the represents an Temperature_INIT. It can be nil.
    ///
    /// - Returns:
    ///   - The Temperature_UNIT that is represented by the Int passed as a parameter.
    static func fromInt(value: Any?) -> Temperature_UNIT? {
        
        guard let value = value as? Int else { return nil }
        
        if value == Temperature_UNIT.Celsius.toInt() {
            return Temperature_UNIT.Celsius
        } else if value == Temperature_UNIT.Farenheit.toInt() {
            return Temperature_UNIT.Farenheit
        }
        return nil
    }
    
    /// Two filePrivate methods.
    /// They are able to convert from Kelvin to Celcius of Farenheit.
    static fileprivate func getCelsiusFromKelvin(_ kelvin: Double) -> Double {
        return kelvin + (-273.15)
    }
    static fileprivate func getFarenheitFromKelvin(_ kelvin: Double) -> Double {
        return (kelvin * (9/5)) - 459.67
    }
}

class WeatherInfo {
    
    private var dateTimestamp: UInt64!
    private var temperature: Double!
    private var temperatureMax: Double!
    private var temperatureMin: Double!
    private var winSpeed: Double!
    var date : Date {
        get {
            return Date.init(timeIntervalSince1970: TimeInterval(self.dateTimestamp))
        }
    }
    
    var weatherDescription: String!
    var cloudiness: Int!
    var rain: Double?
    
    init(timestamp: UInt64, temperature: Double, tempMax: Double, tempMin: Double, windSpeed: Double, weatherDescription: String, cloudiness: Int, rain: Double?) {
        self.dateTimestamp = timestamp
        self.temperature = temperature
        self.temperatureMax = tempMax
        self.temperatureMin = tempMin
        self.winSpeed = windSpeed
        self.weatherDescription = weatherDescription
        self.cloudiness = cloudiness
        self.rain = rain
    }
    
    // MARK: Get helpers. They return information in String format.
    
    public func getTemperature(unit: Temperature_UNIT) -> String {
        switch unit {
        case .Celsius:
            return String(format: "%\(".0")f", Temperature_UNIT.getCelsiusFromKelvin(self.temperature)) + " °C"
        case .Farenheit:
            return String(format: "%\(".0")f", Temperature_UNIT.getFarenheitFromKelvin(self.temperature)) + " F"
        }
    }
    
    public func getMaxMinTemperature(unit: Temperature_UNIT) -> String {
        
        var minTemp: Double!
        var maxTemp: Double!
        var symbol: String = ""
        
        switch unit {
        case .Celsius:
            minTemp = Temperature_UNIT.getCelsiusFromKelvin(self.temperatureMin)
            maxTemp = Temperature_UNIT.getCelsiusFromKelvin(self.temperatureMax)
            symbol = " °C"
        case .Farenheit:
            minTemp = Temperature_UNIT.getFarenheitFromKelvin(self.temperatureMin)
            maxTemp = Temperature_UNIT.getFarenheitFromKelvin(self.temperatureMax)
            symbol = " F"
        }
        return String(format: "Max: %\(".0")f\(symbol)\n Min: %\(".0")f\(symbol)", maxTemp,minTemp)
    }
    
    public func getWindSpeed() -> String {
        return String(format: "%\(".2")f",self.winSpeed) + " mps"
    }
}

