//
//  City.swift
//  weather-app
//
//  Created by Nicolas Yuste Tirados on 10/7/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

public class City {
    let id : Int!
    var name : String!
    
    init(id: Int, name:String) {
        self.id = id
        self.name = name
    }
}

