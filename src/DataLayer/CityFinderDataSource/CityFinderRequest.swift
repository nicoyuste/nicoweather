//
//  CityRequest.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation
import CoreLocation

/// A City Search request that can be executed by CityDataSource
/// It is uses to look for an actual city based on coordinates.
class CityFinderRequest: NSObject, ISAPIClientRequest {
    
    private let location : CLLocation!
    
    // MARK: Init method
    
    /// Creates and returns a new CityRequest that can be executed by CityDataSource
    ///
    /// - Parameters:
    ///   - location: The location we want to use to find a location
    init(location: CLLocation){
        self.location = location
    }
    
    // MARK: - Implementing ISAPIClientRequest Protocol
    
    func getHost() -> String {
        return "https://api.openweathermap.org/"
    }
    func getMethod() -> ISAPIClientMethod {
        return .GET
    }
    func getPath() -> String {
        return "data/2.5/find"
    }
    func getBoby() -> Dictionary<String, Any>? {
        return nil
    }
    func getRequestHeaders() -> Dictionary<String, String>? {
        return nil
    }
    func getQueryParams() -> Dictionary<String, String>? {
        
        var params : Dictionary<String, String> = [:]
        params["lat"] = String(location.coordinate.latitude)
        params["lon"] = String(location.coordinate.longitude)
        params["cnt"] = "1" // We only one location.
        params["appid"] = OPEN_WEATHER_API_ID // TODO: We are going to download this one from Firebase.
        
        return params
    }
}
