//
//  CityDataSourceTests.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import XCTest
import CoreLocation
@testable import weather_app

class CityFinderDataSourceTests: XCTestCase {
    
    var semaphore : DispatchSemaphore? = nil
    let mockRequest = CityFinderRequest(location: CLLocation(latitude: 5, longitude: 5))
    override func setUp() {
        self.semaphore = DispatchSemaphore(value: 0)
    }
    override func tearDown() {
        _ = self.semaphore!.wait(timeout: DispatchTime(uptimeNanoseconds: 1000))
    }
    
    func testDataSourceGoodResponse() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse:testGoodJSONResponse))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            
            XCTAssert(apiResponse.value != nil, "Response value cannot be nil since it should have some response")
            let cities = apiResponse.value!
            XCTAssert(cities.count == 1, "Our test data has two results, there should be two results")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceFailResponse() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: false, jsonResponse:""))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            XCTAssert(apiResponse.error != nil, "Response error needs to be != nil since API is returning an error")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceFailWeirdGoodJSON() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testWeirdJSON))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            XCTAssert(apiResponse.error != nil, "Response error needs to be != nil since API is returning an error")
            self.semaphore?.signal()
        }
        
    }
    func testDataSourceGoodResponseWithMoreThatOneCity() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONResponseTwoCities))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            XCTAssert(apiResponse.value != nil, "Response value cannot be nil since it should have some response")
            let cities = apiResponse.value!
            XCTAssert(cities.count == 1, "Even we have more than 1 successfull city from the API. Datasource should only return 1")
        }
    }
    
    func testDataSourceGoodResponseWithMoreThatOneCityButOneIsMissingCityId() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONResponseTwoCitiesOneMissingId))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            XCTAssert(apiResponse.value != nil, "Response value cannot be nil since it should have some response")
            let cities = apiResponse.value!
            XCTAssert(cities.count == 1, "Our test data has two results, there should be two results")
        }
    }
    
    func testDataSourceGoodResponseWithMoreThatOneCityButOneIsMissingCityName() {
        let dataSource = CityFinderDataSource(apiClient: mockAPIClient(returnSuccess: true, jsonResponse: testGoodJSONResponseTwoCitiesOneMissingName))
        dataSource.executeRequest(request: self.mockRequest) { apiResponse in
            XCTAssert(apiResponse.value != nil, "Response value cannot be nil since it should have some response")
            let cities = apiResponse.value!
            XCTAssert(cities.count == 1, "Our test data has two results, there should be two results")
        }
    }
}

private let testGoodJSONResponse = "{\"message\":\"accurate\",\"cod\":\"200\",\"count\":1,\"list\":[{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Burutu\"}]}"
private let testGoodJSONResponseTwoCities = "{\"message\":\"accurate\",\"cod\":\"200\",\"count\":2,\"list\":[{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Burutu\"},{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2342317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Coladilla\"}]}"
private let testGoodJSONResponseTwoCitiesOneMissingId = "{\"message\":\"accurate\",\"cod\":\"200\",\"count\":1,\"list\":[{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Burutu\"},{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Coladilla\"}]}"
private let testGoodJSONResponseTwoCitiesOneMissingName = "{\"message\":\"accurate\",\"cod\":\"200\",\"count\":1,\"list\":[{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"},\"name\":\"Burutu\"},{\"wind\":{\"speed\":4.76,\"deg\":245.502},\"rain\":null,\"dt\":1507398171,\"id\":2346317,\"coord\":{\"lat\":5.3509,\"lon\":5.5076},\"snow\":null,\"clouds\":{\"all\":88},\"main\":{\"humidity\":95,\"temp_min\":298.972,\"temp_max\":298.972,\"temp\":298.972,\"pressure\":1023.4,\"sea_level\":1023.82,\"grnd_level\":1023.4},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"icon\":\"04n\",\"description\":\"overcast clouds\"}],\"sys\":{\"country\":\"\"}}]}"

