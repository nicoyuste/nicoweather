//
//  CityDataSource.swift
//  weather-app
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation


/// Completion block type that CityDataSource fires when it finishs executing a request.
typealias CityFinderDataSourceCompletionHandler = (Result<[City]>) -> Void

/// This class is able to perform a search into the Open Weather API. It searches for cities based on coordenates.
/// It will perform the API call asynchronously and it will call back into the completion block when finished.
class CityFinderDataSource: NSObject {

    private let apiClient : ISAPIClientProtocol!
    
    /// Creates and returns a new Data Source object that is able to perform a search into Open Weather API.
    /// It requires an APIClient object in order to work. (We do this for testing porpouse so we can mock all the dependencies
    /// this class may have)
    ///
    /// - Parameter apiClient: The apiClient that is going to be able to execute the request.
    init(apiClient: ISAPIClientProtocol) {
        self.apiClient = apiClient
    }
    
    /// It executes a request. It will fire the completion block when it finishs.
    ///
    /// - Parameters:
    ///   - request: The request object that encapsulates all the request information
    ///   - completion: The completion that is going to be executed with finished.
    public func executeRequest(request: CityFinderRequest, completion: @escaping CityFinderDataSourceCompletionHandler)  {
        apiClient.executeRequest(request: request) { apiResponse in
            switch apiResponse {
            case .success:
                if let cities = self.generateCities(jsonCities: apiResponse.value!) {
                    completion(.success(cities))
                } else {
                    completion(.failure(CityDataSourceError.missingCity))
                }
            case .failure:
                completion(.failure(apiResponse.error!))
            }
        }
    }
    
    // MARK: Private helpers
    
    private func generateCities(jsonCities: [String:Any]) -> [City]?{
        guard
            let list = jsonCities["list"] as? [Any],
            let firstCity = list.first as? [String:Any],
            let firstCityId = firstCity["id"] as? Int,
            let firstCityName = firstCity["name"] as? String else {
                return nil
        }
        return [City(id:firstCityId, name: firstCityName)]
    }
}

enum CityDataSourceError : Error {
    case missingCity // Sent in we could not locate the user
}


