//
//  ISAPICLientRequestProtocol.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation

/// Any request that we need to fire using the ISAPIClient will be done using this protocol.
/// You need to call to the method:executeRequest() and pass an object that implements this protocol
protocol ISAPIClientRequest {
    
    /// It returns the HTTP method of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    ///
    /// - Returns: the HTTP method of the request
    func getMethod() -> ISAPIClientMethod
    
    /// It returns the host of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    ///
    /// - Returns: the host of the request
    func getHost() -> String
    
    /// It returns the url path of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    ///
    /// - Returns: the url path of the request
    func getPath() -> String
    
    /// It returns the query params of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    /// This information could be nil since it is not really required for a API Request.
    ///
    /// - Returns: the query params of the request
    func getQueryParams() -> Dictionary<String, String>?
    
    /// It returns the request headers of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    /// This information could be nil since it is not really required for a API Request.
    ///
    /// - Returns: the request headers of the request
    func getRequestHeaders() -> Dictionary<String, String>?
    
    /// It returns the body of a new ISAPIClientRequest.
    /// Any ISAPIClientRequest will need to implement this method.
    /// This information could be nil since it is not really required for a API Request.
    ///
    /// - Returns: the body of the request
    func getBoby() -> Dictionary<String, Any>?
    
}
