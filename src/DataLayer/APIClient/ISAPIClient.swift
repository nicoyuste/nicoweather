//
//  ISAPIClient.swift
//  itunesSearch-test
//
//  Created by Nicolas Yuste on 10/6/17.
//  Copyright © 2017 Nicolas Yuste. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger

/// Defines all the possible HTTP Methods that this API supports.
// TODO: Add more methods than GET, we only needed this for now.
enum ISAPIClientMethod : String {
    case GET = "GET"
    
    internal func toAlamofire() -> Alamofire.HTTPMethod {
        switch self {
        case .GET:
            return HTTPMethod.get
        }
    }
}

/// Completion block that ISAPIClient fires when it finishs executing a request and it is succcesful.
typealias APIClientCompletionHandler = (Result<[String:Any]>) -> Void

/// A protocol that defines how an APIClient should behave.
/// This way, if I ever want to create a new API, I can easily change the implementation and keep the same interface.
protocol ISAPIClientProtocol {
    func executeRequest(request: ISAPIClientRequest, completionHandler: @escaping APIClientCompletionHandler);
}

/// An API Client that is able to execute any type of JSON API request.
/// It will return the JSON object as a Dictionary.
/// We have this class here in order to encapsulate Alamofire. This way, we could change or API Client 3rd party library
///    or even implement our own without impacting all the other classes in the app.
final class ISAPIClient: NSObject, ISAPIClientProtocol {
    
    // Single instance of the API
    public static let shared = ISAPIClient()
    
    // This prevents others from using the default '()' initializer for this class.
    private override init() {
        NetworkActivityLogger.shared.level = .error
        NetworkActivityLogger.shared.startLogging()
    }
    
    /// Asynchronous executes an ISAPIClientRequest request.
    ///
    /// - Parameters:
    ///   - request: The request that is going to be executed.
    ///   - completionHandler: The  block that will be executed when finished
    func executeRequest(request: ISAPIClientRequest, completionHandler: @escaping APIClientCompletionHandler) {
        
        Alamofire.request(request.getHost() + request.getPath(),
                          method: request.getMethod().toAlamofire(),
                          parameters: request.getQueryParams())
            // TODO: Add the body and the headers. This sample app do not need it but this class should handle that.
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    completionHandler(.success(response.result.value as! [String:Any]))
                case .failure:
                    completionHandler(.failure(response.result.error!))
                }
        }
    }
}
